package com.twuc.mallbackend;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.twuc.mallbackend.domain.Commodity;
import com.twuc.mallbackend.service.CommodityService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class CommodityControllerTest {
    @Autowired
    private CommodityService service;
    @Autowired
    private MockMvc mockMvc;

    @Test
    void get_commodity_is_right() throws Exception {
        Commodity cola1 = new Commodity("Cola1", 1L, "瓶", "http");
        Commodity commodity = service.createCommodity(cola1);
        String asString = new ObjectMapper().writeValueAsString(cola1);
        mockMvc.perform(get("/api/commodity")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(asString))
                .andExpect(status().isOk());

    }

    @Test
    void get_commodity_id_is_right() throws Exception {
        Commodity cola1 = new Commodity("Cola1", 1L, "瓶", "http");
        Commodity commodity = service.createCommodity(cola1);
        String asString = new ObjectMapper().writeValueAsString(cola1);
        mockMvc.perform(get("/api/commodity/1")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(asString))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value("Cola1"));

    }
}
