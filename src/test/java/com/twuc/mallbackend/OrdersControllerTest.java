package com.twuc.mallbackend;

import com.twuc.mallbackend.domain.Commodity;
import com.twuc.mallbackend.domain.CommodityRepository;
import com.twuc.mallbackend.domain.Orders;
import com.twuc.mallbackend.domain.OrdersRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;


@SpringBootTest
public class OrdersControllerTest {

    @Autowired
    private CommodityRepository commodityRepository;

    @Autowired
    private OrdersRepository ordersRepository;

    @Test
    void should_add_order_success() {
        Commodity commodity = new Commodity("pingguo", 2L, "kilometer", "http");
        commodityRepository.save(commodity);
        Orders orders = new Orders(1L, 3);
        ordersRepository.save(orders);

        Orders orders1 = ordersRepository.findById(1L).get();
        assertEquals(orders1.getAmount(), 3);
    }
}
