ALTER TABLE orders
ADD FOREIGN KEY fk_orders_commodity(commodity_id)
REFERENCES commodity(id)
ON DELETE RESTRICT
ON UPDATE RESTRICT