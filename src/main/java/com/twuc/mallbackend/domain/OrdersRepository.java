package com.twuc.mallbackend.domain;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

@Component
public interface OrdersRepository extends JpaRepository<Orders, Long> {
    Orders findByCommodityId(long commodityId);
}
