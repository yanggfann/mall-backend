package com.twuc.mallbackend.domain;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "commodity")
public class Commodity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name", nullable = false, length = 64)
    @NotNull
    private String name;

    @Column(name = "price", nullable = false)
    @NotNull
    private Long price;

    @Column(name = "unit", nullable = false)
    @NotNull
    private String unit;

    @Column(name = "image", nullable = false)
    @NotNull
    private String image;

    public Commodity() {
    }

    public Commodity(@NotNull String name, @NotNull Long price, @NotNull String unit, @NotNull String image) {
        this.name = name;
        this.price = price;
        this.unit = unit;
        this.image = image;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Long getPrice() {
        return price;
    }

    public String getUnit() {
        return unit;
    }

    public String getImage() {
        return image;
    }
}
