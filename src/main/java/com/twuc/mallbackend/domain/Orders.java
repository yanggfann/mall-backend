package com.twuc.mallbackend.domain;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "orders")
public class Orders {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne
    private Commodity commodity;

    @Column(name = "amount")
    private int amount;

    public Orders() {
    }

    public Orders(Commodity commodity) {
        this.commodity = commodity;
        this.amount = 1;
    }

    public Long getId() {
        return id;
    }

    public Commodity getCommodity() {
        return commodity;
    }

    public void setCommodity(Commodity commodity) {
        this.commodity = commodity;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
}
