package com.twuc.mallbackend.service;

import com.twuc.mallbackend.domain.Commodity;
import com.twuc.mallbackend.domain.CommodityRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class CommodityService {

    private final CommodityRepository repository;

    public CommodityService(CommodityRepository repository) {
        this.repository = repository;
    }



    public Commodity findCommodity(Long commodity_id){
        Optional<Commodity> commodityById = repository.findById(commodity_id);
        return commodityById.orElse(null);
    }
    public ResponseEntity getResponseEntityById(Commodity commodity) {
        if(commodity!=null){
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body(commodity);
        }
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .build();
    }

    public List<Commodity> findALl() {
        return repository.findAll();
    }
    public ResponseEntity getResponseEntity(List<Commodity> commodity) {
        if(commodity!=null){
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body(commodity);
        }
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .build();
    }

    public Commodity createCommodity(Commodity cola) {
        return repository.save(cola);
    }
}
