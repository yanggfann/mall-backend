package com.twuc.mallbackend.service;

import com.twuc.mallbackend.domain.Commodity;
import com.twuc.mallbackend.domain.CommodityRepository;
import com.twuc.mallbackend.domain.Orders;
import com.twuc.mallbackend.domain.OrdersRepository;
import com.twuc.mallbackend.exception.NotHasOrderException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class OrdersService {

    private final OrdersRepository ordersRepository;
    private final CommodityRepository commodityRepository;

    public OrdersService(OrdersRepository ordersRepository, CommodityRepository commodityRepository) {
        this.ordersRepository = ordersRepository;
        this.commodityRepository = commodityRepository;
    }


    public void createOrder(Long commodity_id) {
        Orders orders = ordersRepository.findByCommodityId(commodity_id);
        if(orders!=null){
            orders.setAmount(orders.getAmount()+1);
            ordersRepository.save(orders);
        }else{
            Commodity commodity = commodityRepository.findById(commodity_id).get();
            ordersRepository.save(new Orders(commodity));
        }
    }

    public List<Orders> findOrder() {
        return ordersRepository.findAll();
    }


    public List<Orders> deleteById(Long order_id) {
        Orders order = ordersRepository.findById(order_id).orElseThrow(() -> new NotHasOrderException("order not exist"));
        ordersRepository.delete(order);
        return ordersRepository.findAll();
    }
}
