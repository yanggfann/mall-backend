package com.twuc.mallbackend.web;

import com.twuc.mallbackend.domain.Orders;
import com.twuc.mallbackend.domain.OrdersRepository;
import com.twuc.mallbackend.service.OrdersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*")
public class OrdersController {

    private final OrdersService service;

    public OrdersController(OrdersService service) {
        this.service = service;
    }

    @PostMapping("/order/{commodity_id}")
    public ResponseEntity createOrder(@PathVariable Long commodity_id){
        service.createOrder(commodity_id);
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .build();
    }

    @GetMapping("/order")
    public ResponseEntity getOrder(){
        List<Orders> order = service.findOrder();
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(order);
    }

    @DeleteMapping("/order/{order_id}")
    public ResponseEntity deleteOrder(@PathVariable Long order_id){
        List<Orders> orders = service.deleteById(order_id);
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(orders);
    }
}
