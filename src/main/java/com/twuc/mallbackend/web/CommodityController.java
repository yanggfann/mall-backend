package com.twuc.mallbackend.web;

import com.twuc.mallbackend.domain.Commodity;
import com.twuc.mallbackend.service.CommodityService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
@RequestMapping("/api")
//跨域资源共享，解决前端无法获取后端数据的问题
@CrossOrigin(origins = "*")
public class CommodityController {

    private final CommodityService service;

    public CommodityController(CommodityService service) {
        this.service = service;
    }

    @GetMapping("/commodity")
    public ResponseEntity getCommodity(){
        List<Commodity> commodity = service.findALl();
        return service.getResponseEntity(commodity);
    }

    @GetMapping("/commodity/{commodity_id}")
    public ResponseEntity getCommodity(@PathVariable Long commodity_id){
        Commodity commodity = service.findCommodity(commodity_id);
        return service.getResponseEntityById(commodity);
    }

    @PostMapping("/commodity")
    public ResponseEntity createCommodity(@RequestBody @Valid Commodity commodity){
        Commodity commodity1 = service.createCommodity(commodity);
        return ResponseEntity
                .created(linkTo(methodOn(CommodityController.class).getCommodity(commodity1.getId())).toUri())
                .build();
    }
}
