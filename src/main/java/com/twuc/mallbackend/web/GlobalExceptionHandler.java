package com.twuc.mallbackend.web;

import com.twuc.mallbackend.exception.NotHasOrderException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler({NotHasOrderException.class})
    public ResponseEntity handleNotHasException (Exception e){
        return ResponseEntity
                .status(404)
                .body(e.getMessage());
    }
}
